Middlecoin watcher
===============
by webbson


A small stat page for the [Middlecoin](http://middlecoin.com/) altcoin scrypt pool. 
You can find it live and running here: [vps.webbson.net/middlecoin](http://vps.webbson.net/middlecoin)

Check out the [issues](https://bitbucket.org/webbson/middlecoin-watcher/issues?kind=enhancement&status=new&status=open) page for coming feautures.
If you find any bugs or want to request a feature use the [issue tracker](https://bitbucket.org/webbson/middlecoin-watcher/issues/new)

If you like this page please donate to keep the servers running,

BTC: 1W3BBSNm3KMtvtMoARiYGjzEoRHB66P69
or 
DOGE: DTWxZcMatr6W9LL8bVazWJVNjcJV6YTC4m