<?php
    ob_start("ob_gzhandler");
    $db = new PDO('mysql:host=localhost;dbname=middlecoin;charset=utf8;', 'mcviewer', '5p7wbz3nRtaXcNSv');
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    switch ($_GET['do'])
    {
        case 'getDataForMiner':
            getDataForMiner();
            break;

        case 'addEmailWatch':
            addEmailWatch();
            break;

        case 'removeEmailWatch':
            removeEmailWatch();
            break;

        case 'getBtcPerMhs':
            getBtcPerMhs();
            break;

        default:
           send404();
    }

    function getDataForMiner(){
        global $db;
        $minerId = getMinerId();
        $offset = isset($_GET['offset']) ? ' '.intval($_GET['offset']).' hour' : '';
        $textonly = isset($_GET['textonly']) ? $_GET['textonly'] : false;
        $days = isset($_GET['days']) ? intval($_GET['days']) : 28;
        if (file_exists('cache/stats-'.$minerId.'-'.$days.'-'.$offset.'-'.($textonly ? 'text' : 'graphs').'.json'))
		{
			$cache = file_get_contents('cache/stats-'.$minerId.'-'.$days.'-'.$offset.'-'.($textonly ? 'text' : 'graphs').'.json');
		} else {
            $stmt = $db->prepare('
                SELECT
                    I.timestamp,
                    S.*,
                    ( SELECT ROUND(AVG(S2.megahashesPerSecond),2)
                    FROM
                        imports I2
                    LEFT JOIN
                        stats S2 ON I2.id = S2.import AND S2.miner = :miner
                    WHERE
                        I2.id <= I.id
                    AND
                        I2.timestamp >= (I.timestamp - 10800)
                    ) AS megahashesPerSecond3hrAverage,
                    I.totalImmatureBalance,
                    I.totalUnexchangedBalance,
                    I.totalBalance,
                    I.totalMegahashesPerSecond,
                    I.totalRejectedMegahashesPerSecond
                FROM
                    stats S
                LEFT JOIN
                    imports I ON I.id = S.import
                WHERE
                    S.miner = :miner AND I.timestamp >= (UNIX_TIMESTAMP() - (:days * 86400) - 14400)
                ORDER BY S.import '.($textonly ? 'DESC LIMIT 1' : 'ASC'));
            $stmt->bindValue(':miner', $minerId, PDO::PARAM_INT);
            $stmt->bindValue(':days', $days, PDO::PARAM_INT);
            $stmt->execute();
            $results = $stmt->fetchAll();
            if (!$textonly)
            {
                $cols_btc = array(
                array('label' => 'Date', 'type' => 'datetime'),
                array('label' => 'Immature', 'type' => 'number'),
                array('label' => 'Unexchanged', 'type' => 'number'),
                array('label' => 'Balance', 'type' => 'number'),
                array('label' => 'Pool immature', 'type' => 'number'),
                array('label' => 'Pool unexchanged', 'type' => 'number'),
                array('label' => 'Pool balance', 'type' => 'number'));
                $rows_btc = array();
                $cols_rate = array(
                    array('label' => 'Date', 'type' => 'datetime'),
                    array('label' => 'Rejected', 'type' => 'number'),
                    array('label' => 'Accepted', 'type' => 'number'),
                    array('label' => '3 hour moving average', 'type' => 'number'),
                    array('label' => 'Pool rejected', 'type' => 'number'),
                    array('label' => 'Pool accepted', 'type' => 'number'));
                $rows_rate = array();


                foreach($results AS $result)
                {
                    $date = date("Y-m-d H:i:s", strtotime('@'.$result['timestamp'].$offset));
                    $casArr = explode(' ', $date); // split into date and time segments
                    $dateArr = explode('-', $casArr[0]); // split date into year, month, day
                    $timeArr = explode(':', $casArr[1]); // split time into hour, minute, second
                    $date = "Date($dateArr[0], ".(((int) $dateArr[1]) - 1).", $dateArr[2], $timeArr[0], $timeArr[1], $timeArr[2])";
                    array_push($rows_btc, array('c' => array(array('v' => $date), array('v' => $result['immatureBalance']), array('v' => $result['unexchangedBalance']), array('v' => $result['bitcoinBalance']), array('v' => $result['totalImmatureBalance']), array('v' => $result['totalUnexchangedBalance']), array('v' => $result['totalBalance']))));
                    array_push($rows_rate, array('c' => array(array('v' => $date), array('v' => $result['rejectedMegahashesPerSecond'], 'f' => mhsString($result['rejectedMegahashesPerSecond'], ' ('.$result['lastHourRejectedShares'].' shares)')), array('v' => $result['megahashesPerSecond'], 'f' => mhsString($result['megahashesPerSecond'], ' ('.$result['lastHourShares'].' shares)')), array('v' => $result['megahashesPerSecond3hrAverage'], 'f' => mhsString($result['megahashesPerSecond3hrAverage'])), array('v' => $result['totalRejectedMegahashesPerSecond'], 'f' => mhsString($result['totalRejectedMegahashesPerSecond'])), array('v' => $result['totalMegahashesPerSecond'], 'f' => mhsString($result['totalMegahashesPerSecond'])))));
                }
            }
            $stats = end($results);
            $stats['timestamp'] = date("Y-m-d H:i:s", strtotime('@'.$stats['timestamp'].$offset));
            $cache = json_encode(array(
                'graphs' => array(
                    'btc' => array('cols' => $cols_btc, 'rows' => $rows_btc),
                    'rate' => array('cols' => $cols_rate, 'rows' => $rows_rate)),
                'stats' => $stats,
                'apiVersion' => 0.2),
                true);
            $fh = @fopen('cache/stats-'.$minerId.'-'.$days.'-'.$offset.'-'.($textonly ? 'text' : 'graphs').'.json', 'wb');
            if (!$fh)
                exit('Unable to cache directory. Please make sure PHP has write access.');
            fwrite($fh, $cache);
            fclose($fh);
        }
        exit($cache);

    }

    function getBtcPerMhs()
    {
        global $db;
        if (!isset($_GET['minerId']))
           send406('minerId');
        $offset = isset($_GET['offset']) ? ' '.intval($_GET['offset']).' hour' : '';
        $days = isset($_GET['days']) ? intval($_GET['days']) : 28;
        $minerId = intval($_GET['minerId']);
        if (file_exists('cache/btcmhs-'.$minerId.'-'.$days.'-'.$offset.'.json'))
		{
			$cache = file_get_contents('cache/btcmhs-'.$minerId.'-'.$days.'-'.$offset.'.json');
		} else {
            // BTC PER MHS
            $stmt = $db->prepare('
                SELECT
                    ROUND(IF(@lastValue > 0, S.paidOut - @lastValue,0), 8) AS paid,
                    ROUND(IF(@lastValue > 0, S.paidOut - @lastValue,0)/avg(S.megahashesPerSecond), 8) AS btcPerMHs,
                    AVG(S.megahashesPerSecond) AS MHs,
                    ROUND(IF(@lastPool > 0, I.totalPaidOut - @lastPool,0)/avg(I.totalMegahashesPerSecond), 8) AS poolBtcPerMHs,
                    ROUND(IF(@lastPool > 0, I.totalPaidOut - @lastPool,0), 8) AS poolPaid,
                    I.timestamp,
                    @lastValue:=S.paidOut AS paidOut,
                    @lastPool:=I.totalPaidOut AS poolPaidOut
                FROM
                    stats S
                LEFT JOIN
                    imports I ON S.import = I.id,
                (SELECT @lastValue:=0.0, @lastPool:=0.0) T
                WHERE
                    S.miner =:miner AND I.timestamp >= (UNIX_TIMESTAMP() - (:days * 86400) - 14400) GROUP BY S.paidOut');
            $stmt->bindValue(':miner', $minerId, PDO::PARAM_INT);
            $stmt->bindValue(':days', $days, PDO::PARAM_INT);
            $stmt->execute();
            $results = $stmt->fetchAll();
            $cols_btcmhs = array(
                array('label' => 'Date', 'type' => 'datetime'),
                array('label' => 'Paid', 'type' => 'number'),
                array('label' => 'BTC/MHs', 'type' => 'number'),
                array('label' => 'Pool BTC/MHs', 'type' => 'number'));
            $rows_btcmhs = array();
            foreach($results as $result)
            {
                $date = date("Y-m-d H:i:s", strtotime('@'.$result['timestamp'].$offset));
                $casArr = explode(' ', $date); // split into date and time segments
                $dateArr = explode('-', $casArr[0]); // split date into year, month, day
                $timeArr = explode(':', $casArr[1]); // split time into hour, minute, second
                $date = "Date($dateArr[0], ".(((int) $dateArr[1]) - 1).", $dateArr[2], $timeArr[0], $timeArr[1], $timeArr[2])";
                array_push($rows_btcmhs, array('c' => array(array('v' => $date), array('v' => $result['paid']), array('v' => $result['btcPerMHs']), array('v' => $result['poolBtcPerMHs']))));
            }
            $cache = json_encode(array('cols' => $cols_btcmhs, 'rows' => $rows_btcmhs, 'raw' => $results));
            $fh = @fopen('cache/btcmhs-'.$minerId.'-'.$days.'-'.$offset.'.json', 'wb');
            if (!$fh)
                exit('Unable to cache directory. Please make sure PHP has write access.');
            fwrite($fh, $cache);
            fclose($fh);
        }
        exit($cache);
    }
        
    function addEmailWatch()
    {
        global $db;
        $minerId = getMinerId();
        if (!isset($_GET['threshold']))
           send406('threshold');
        if (!isset($_GET['email']))
           send406('email');
        $stmt = $db->prepare('INSERT INTO email VALUES(:miner,:threshold,:email,null) ON DUPLICATE KEY UPDATE threshold = :threshold, email = :email');
        $stmt->bindValue(':miner', $minerId, PDO::PARAM_INT);
        $stmt->bindValue(':threshold', $_GET['threshold']);
        $stmt->bindValue(':email', $_GET['email']);
        exit($stmt->execute());
    }

    function removeEmailWatch()
    {
        global $db;
        $minerId = getMinerId();
        if (!isset($_GET['email']))
           send406('email');
        $stmt = $db->prepare('DELETE FROM email WHERE miner=:miner AND email LIKE :email');
        $stmt->bindValue(':miner', $minerId, PDO::PARAM_INT);
        $stmt->bindValue(':email', $_GET['email']);
        exit($stmt->execute());
    }

    function getMinerId()
    {
        if (!isset($_GET['address']))
           send406('address');
        if (!checkAddress($_GET['address']))
            send400('Invalid address');
        global $db;
        // Check for valid miner
        $stmt = $db->prepare('SELECT id FROM miners WHERE address LIKE ? LIMIT 1');
        $stmt->bindValue(1, $_GET['address']);
        $stmt->execute();
        $minerId = intval($stmt->fetchColumn());
        if ($minerId == 0) send400('Address not in database');
        return $minerId;
    }

    function checkAddress($address)
    {
        $origbase58 = $address;
        $dec = "0";
    
        for ($i = 0; $i < strlen($address); $i++)
            $dec = bcadd(bcmul($dec,"58",0),strpos("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz",substr($address,$i,1)),0);
            
        $address = "";
    
        while (bccomp($dec,0) == 1)
        {
            $dv = bcdiv($dec,"16",0);
            $rem = (integer)bcmod($dec,"16");
            $dec = $dv;
            $address = $address.substr("0123456789ABCDEF",$rem,1);
        }
    
        $address = strrev($address);
    
        for ($i = 0; $i < strlen($origbase58) && substr($origbase58,$i,1) == "1"; $i++)
            $address = "00".$address;
    
        if (strlen($address)%2 != 0)
            $address = "0".$address;
            
        if (strlen($address) != 50 || hexdec(substr($address,0,2)) > 0)
            return false;
        
        return substr(strtoupper(hash("sha256",hash("sha256",pack("H*",substr($address,0,strlen($address)-8)),true))),0,8) == substr($address,strlen($address)-8);
    }

    function mhsString($input, $append = '')
    {
        return empty($input) ? null : $input.' MH/s'.$append;
    }
           
    function send400($reason)
    {
        header('HTTP/1.0 400 Bad Request');
        exit('Error 400 - Reason: '.$reason);
    }
    
    function send403($reason)
    {
        header('HTTP/1.0 403 Forbidden');
        exit('Error 403 - Reason: '.$reason);
    }
    
    function send404()
    {
        header('HTTP/1.0 404 Not Found');
        exit('404');
    }
    
    function send406($missingParameters)
    {
        header('HTTP/1.0 406 Not Acceptable');
        exit('Error 406 - Method expects parameters: '.$missingParameters);
    }
    
    function send500($reason)
    {
        header('HTTP/1.0 500 Internal Server Error');
        exit('Error 500 - Reason: '.$reason);
    }
