/*global $, console, document, window, setInterval, clearInterval, google, location, alert, history, setTimeout, ActiveXObject, navigator */
google.load('visualization', '1.0', {
    'packages': ['corechart']
});
var browser = {};
browser.isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;  // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
browser.isFirefox = typeof InstallTrigger !== 'undefined';  // Firefox 1.0+
browser.isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;   // At least Safari 3+: "[object HTMLElementConstructor]"
browser.isChrome = !!window.chrome && !browser.isOpera; // Chrome 1+
browser.isIE = /*@cc_on!@*/false || document.documentMode;  // At least IE6
browser.isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent); // Is mobile
browser.isIOS = /iPhone|iPad|iPod/i.test(navigator.userAgent); // Is iOS
browser.isAndroid = /Android/i.test(navigator.userAgent); // Is Android

$(function () {
    if (getQueryVariable('wallet') === null) {
        history.pushState({}, 'title', window.location.origin+window.location.pathname+'?wallet=1JTgncSrWiQtEAdtYhtytCEqND8wVb3b5R');
    }
    var $info = $('#info'),
    $h2 = $('h2', $info),
    $paid = $('#paid', $info),
    $next = $('#nextpay', $info),
    $curr = $('#curr', $info),
    $refresh = $('#refresh').click(function () { updateNow = true; renderPage();}),
    $import = $('#import', $info),
    $unex = $('#unex', $info),
    $imm = $('#imm', $info),
    $shares = $('#shares', $info),
    $mhs = $('#mhs', $info),
    $mhs3hr = $('#mhs3hr', $info),
    $rate = $('#rate', $info ),
    $1daymhs = $('#1daymhs', $info),
    $weekmhs = $('#1weekmhs', $info),
    $1daybtcmhs = $('#1daybtcmhs', $info),
    $weekbtcmhs = $('#1weekbtcmhs', $info),
    $weeksmhs = $('#weeksmhs', $info),
    $weeksbtcmhs = $('#weeksbtcmhs', $info),
    $darkness = $('#darkness'),
    counter = null,
    lastpaid = 0,
    offset = getQueryVariable('offset') === null ? 0 : tryParseInt(getQueryVariable('offset'), 0),
    wallet = getQueryVariable('wallet'),
    currency = getQueryVariable('currency') === null ? 'usd' : getQueryVariable('currency'),
    theme = getQueryVariable('theme') === null ? 'default' : getQueryVariable('theme'),
    exchanges = getQueryVariable('exchanges') === null ? ['bitstamp','btce','localbitcoins','mtgox'] : getQueryVariable('exchanges').split(','),
    textOnly = getQueryVariable('textonly') === null ? false : (getQueryVariable('textonly') === 'true'),
    days = tryParseInt(getQueryVariable('days'), 7),
    isActive = true,
    showPool = true,
    updateNow = true,
    apiVersion = null,
    cache = null,
    width = 1000,
    btc_chart = new google.visualization.ComboChart(document.getElementById('btc_chart')),
    rate_chart = new google.visualization.ComboChart(document.getElementById('rate_chart')),
    btcmhs_chart = new google.visualization.ComboChart(document.getElementById('btcmhs_chart')),
    btc_options, rate_options, btcmhs_options;

    initialize();
    function initialize() {
        $('meta[name=viewport]').remove();
        lastpaid = 0;
        switch(theme) {
            case 'mobile':
                $('head').append('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">');
                width= $('body').width() - 10;
                break;

            case 'wide':
                width = $info.width() -40;
                break;
            default:
                width = 1000;
        }
        btc_options = {
                'title': 'BTC over time',
                'width': width,
                'height': 400,
                isStacked: true,
                colors: ['orange', 'green', 'black', 'red', 'blue', 'gray'],
                legend: {
                    position: 'bottom',
                    textStyle: {
                        color: 'black',
                        fontSize: 14
                    },
                    alignment: 'center'
                },
                chartArea: {
                    left: 0,
                    top: 20,
                    width: width,
                    height: 330
                },
                seriesType: 'area',
                focusTarget: 'category',
                lineWidth: 1,
                vAxis: {textPosition: 'in'},
                hAxis: {textPosition: 'out'},
                series: {2: {type: "line", isStacked: false, lineWidth: 2},3: {type: "line", targetAxisIndex: 1, lineWidth: 1},4: {type: "line", targetAxisIndex: 1, lineWidth: 1},5: {type: "line", isStacked: false, targetAxisIndex: 1, lineWidth: 1}},
            };
        rate_options = {
                'title': 'MH/s per hour',
                'width': width,
                'height': 150,
                isStacked: true,
                colors: ['red', 'green', 'blue', 'pink', 'lightgreen'],
                legend: {
                    position: 'none'
                },
                chartArea: {
                    left: 0,
                    top: 25,
                    width: width,
                    height: 120
                },
                titlePosition: 'top',
                seriesType: 'area',
                focusTarget: 'category',
                lineWidth: 1,
                vAxis: {textPosition: 'in'},
                series: {2: {type: "line", isStacked: false, lineWidth: 2}, 3: {type: "line", targetAxisIndex: 1, lineWidth: 1},4: {type: "line", targetAxisIndex: 1, lineWidth: 1}}
            };
        btcmhs_options = {
                'title': 'Pay history and BTC/MHs',
                'width': width,
                'height': 100,
                colors: ['green', 'red', 'gray'],
                legend: {
                    position: 'none',
                },
                chartArea: {
                    left: 80,
                    top: 15,
                    width: width-150,
                    height: 80
                },
                titlePosition: 'none',
                focusTarget: 'category',
                seriesType: 'bars',
                series: {1: {type: "line", targetAxisIndex: 1, curveType: 'function'},2: {type: "line", targetAxisIndex: 1, curveType: 'function'}},
                vAxes:[
                    {title: "Paid BTC"}, // Nothing specified for axis 0
                    {title: 'BTC/MHs'} // Axis 1
                ],
                hAxis: {textPosition: 'in'}
            };
    }

    window.onfocus = function () { 
      isActive = true;
    }; 
        
    window.onblur = function () { 
      isActive = false; 
    };
    if (!browser.isMobile) $('#fullscreen').click(function(){
        var requestMethod = $info[0].requestFullScreen || $info[0].webkitRequestFullScreen || $info[0].mozRequestFullScreen || $info[0].msRequestFullScreen;
        if ($info[0].webkitRequestFullScreen) {
            $info[0].webkitRequestFullScreen($info[0].ALLOW_KEYBOARD_INPUT);
            setTimeout(function() {
                if (!document.webkitCurrentFullScreenElement)
                    $info[0].webkitRequestFullScreen();
            },250);
        } else if (requestMethod) {
            requestMethod.call($info[0]);
        } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
            var wscript = new ActiveXObject("WScript.Shell");
            if (wscript !== null) {
                wscript.SendKeys("{F11}");
            }
        }
    });
    else
        $('#fullscreen').hide();
    $('#settings').click(function(){
            $darkness.find('#miner').val(wallet);
            $darkness.find('#currency').val(currency);
            $darkness.find('#offset').val(offset);
            $darkness.find('#theme').val(theme);
            $darkness.find('#days').val(days);
            if(textOnly)
                $darkness.find('#textonly').attr('checked','checked');
            $.each(exchanges, function(){
                $darkness.find('#'+this).attr('checked','');
            });
            $('#settingsDiv', $darkness).show();
            $darkness.fadeIn();
    }).mouseenter(function(){$(this).find('i').addClass('fa-spin');}).mouseout(function(){$(this).find('i').removeClass('fa-spin');});
    $('#emailicon').click(function(){
            $('#emailDiv', $darkness).show();
            $darkness.fadeIn();
    });
    $('#pool').click(function(){
        showPool = !showPool;
        renderPage();
    });
    $('#about').click(function(){
        $('#aboutDiv', $darkness).show();
        $darkness.fadeIn();
    });
    
    $('#saveSettings', $darkness).click(function(){
        if (wallet != $darkness.find('#miner').val() || offset != tryParseInt($darkness.find('#offset').val(), 0) || days != $('#days').val())
            updateNow = true;
        wallet = $darkness.find('#miner').val();
        currency = $darkness.find('#currency').val().toLowerCase();
        offset = tryParseInt($darkness.find('#offset').val(), 0);
        exchanges = [];
        theme = $darkness.find('#theme').val();
        textOnly = $('#textonly').is(':checked');
        days = $('#days').val();
        $.each($('#darkness').find('input[type=checkbox]:checked'), function(){
            exchanges.push($(this).val());
        });
        var query = '';
        if (exchanges.length === 0)
            query += '&exchanges=none';
        else {
            if (currency !== 'usd')
                query += '&currency='+currency;
            if (exchanges.length < 4)
                query += '&exchanges='+exchanges.join(',');
        }
        if (offset !== 0)
            query += '&offset='+offset;
        if (theme !== 'default')
            query += '&theme='+theme;
        if (textOnly)
            query += '&textonly=true';
        if (days < 28)
            query += '&days='+days;
        history.pushState({}, 'title', window.location.origin+window.location.pathname+'?wallet='+wallet+query);
        initialize();
        renderPage();
        $darkness.fadeOut();
        $('#settingsDiv', $darkness).fadeOut();
    });
    $('#cancelSettings', $darkness).click(function(){
        $darkness.fadeOut();
        $darkness.find('input[type=checkbox]').removeAttr('checked');
        $('#settingsDiv', $darkness).fadeOut();
    });
    $('#closeAbout', $darkness).click(function(){
        $darkness.fadeOut();
        $('#aboutDiv', $darkness).fadeOut();
    });
    $('#saveEmail', $darkness).click(function(){
        //Todo validate
        var threshold = $('#threshold', $darkness).val(),
            email = $('#email', $darkness).val();
            threshold = tryParseFloat(threshold, 0);
        if (!validateEmail(email)) {
            alert('Invalid email');
            return;
        }
        $.post('api.php?do=addEmailWatch&address=' + wallet + '&email=' + email + '&threshold=' + threshold).fail(function(e) {
            alert( e.responseText );
        });
        $darkness.fadeOut();
        $('#emailDiv', $darkness).fadeOut();
    });
    $('#cancelEmail', $darkness).click(function(){
        $darkness.fadeOut();
        $('#emailDiv', $darkness).fadeOut();
    });
    $darkness.click(function(e){
        if(e.target != this) return;
        $darkness.fadeOut().find('div').fadeOut();
    });

    renderPage();
    updatePageInterval();
    
    function updatePageInterval() {
        var buildInterval = setInterval(function () {
            updateNow = true;
            if (isActive) 
                renderPage();
            else {
                clearInterval(buildInterval);
                $(window).on('focus.build', function(){
                    renderPage();
                    updatePageInterval();
                    $(window).unbind('focus.build');
                });
            }
        }, 600000);
    }

    function renderPage() {
        $('body').removeClass().addClass(theme);
        $h2.empty().append($('<a>', {
            href: 'http://middlecoin.com/reports/' + wallet + '.html',
            target: '_blank',
            text: wallet
        }));

        if (updateNow) {
            if (counter !== null)
                clearInterval(counter);
            $refresh.html('<i class="fa fa-refresh fa-spin"></i> Updating... please wait...');
            $.getJSON('api.php?do=getDataForMiner&address=' + wallet + (offset === 0 ? '': ('&offset='+offset)) + (days === 28 ? '': ('&days='+days)), function (response) {
                if (apiVersion !== null && apiVersion !== response.apiVersion)
                    location.reload(true);
                else 
                    apiVersion = response.apiVersion;
                cache = response;
                updateNow = false;
                renderData();
                var count = 0;
                counter = setInterval(function () {
                    var sec = count%60;
                    var min = parseInt((count++)/60);
                    $refresh.text('Time since refresh ' + min + ':' + (sec < 10 ? '0' : '') + sec);
                }, 1000);
            }).error(function(e) {
                if (e.status == 404)
                    location.reload(true);
                else
                    alert('Something went wrong: '+e.responseText);
            });
        } else
            renderData();
    }

    function renderData() {
        var w = cache.stats;
            $paid.text(w.paidOut);
            $next.text((tryParseFloat(w.immatureBalance, 0) + tryParseFloat(w.unexchangedBalance, 0) + tryParseFloat(w.bitcoinBalance, 0)).toFixed(8));
            $curr.text(w.bitcoinBalance);
            $unex.text(w.unexchangedBalance);
            $imm.text(w.immatureBalance);
            $shares.text(tryParseInt(w.lastHourShares, 0) + '/' + (tryParseInt(w.lastHourShares, 0) + tryParseInt(w.lastHourRejectedShares, 0)) + ' (' + ((tryParseInt(w.lastHourShares, 0) / (tryParseInt(w.lastHourShares, 0) + tryParseInt(w.lastHourRejectedShares, 0))) * 100).toFixed(2) + '%)');
            $mhs.text(tryParseFloat(w.megahashesPerSecond, 0) + ' / ' + tryParseFloat(w.rejectedMegahashesPerSecond, 0));
            $mhs3hr.text(tryParseFloat(w.megahashesPerSecond3hrAverage, 0) + ' MHs');
            $import.text('JSON timestamp: ' + w.timestamp + ' - ');
            getExchangeRates();
            if (!textOnly) {
                $('#btc_chart, #rate_chart, #btcmhs_chart, #pool').show();
                btc_chart.clearChart();
                rate_chart.clearChart();
                var btc_data = new google.visualization.DataTable(cache.graphs.btc);
                var btc_view = new google.visualization.DataView(btc_data);
                if (!showPool) {
                    btc_view.setColumns([0,1,2,3]);
                }

                btc_chart.draw(btc_view, btc_options);
                var rate_data = new google.visualization.DataTable(cache.graphs.rate);
                var rate_view = new google.visualization.DataView(rate_data);
                if (!showPool) {
                    rate_view.setColumns([0,1,2,3]);
                }

                rate_chart.draw(rate_view, rate_options);
            } else {
                $('#pool, #btc_chart, #rate_chart, #btcmhs_chart').hide();
            }
            if (lastpaid != w.paidOut) {
                $.getJSON('api.php?do=getBtcPerMhs&minerId=' + w.miner + (offset === 0 ? '': ('&offset='+offset)), function (response) {
                    if (!textOnly) {
                        btcmhs_chart.clearChart();
                        var btcmhs_data = new google.visualization.DataTable(response);
                        btcmhs_chart.draw(btcmhs_data, btcmhs_options);
                    }
                    var transaction = response.raw.pop(), btcPerMHs = 0.0, MHs = 0.0, count = 0;
                    $1daymhs.text(tryParseFloat(transaction.MHs, 0).toFixed(2));
                    $1daybtcmhs.text(tryParseFloat(transaction.btcPerMHs, 0).toFixed(5));
                    while (transaction.btcPerMHs > 0 && count++ < 21) {
                        console.log(count);
                        btcPerMHs += tryParseFloat(transaction.btcPerMHs, 0);
                        MHs += tryParseFloat(transaction.MHs, 0);
                        if (count === 7 ) {
                            $weekbtcmhs.text((btcPerMHs/7.0).toFixed(5));
                            $weekmhs.text((MHs/7.0).toFixed(2));
                        } else if (count === 21) {
                            $weeksbtcmhs.text((btcPerMHs/21.0).toFixed(5));
                            $weeksmhs.text((MHs/21.0).toFixed(2));
                        }
                        transaction = response.raw.pop();
                    }
                    lastpaid = w.paidOut;
                });
            }
    }

    function getExchangeRates() {
        if (exchanges.length === 0 || exchanges[0] === 'none')
            $rate.parents('div.info').css('visibility','hidden');
        else
            $.getJSON('jsonproxy.php?url=http://preev.com/pulse/source:' + exchanges.join(',') + '/unit:btc,' + currency, null, function (data) {
                var exchangeRate = 0, paid = tryParseFloat($paid.text(), 0), next = tryParseFloat($next.text(), 0), failed = 0;
                $.each(exchanges, function(){
                    exchangeRate += tryParseFloat( data.contents.markets[this].price, 0);
                    if (tryParseFloat( data.contents.markets[this].price, 0) === 0)
                        failed++;
                });
                exchangeRate = (exchangeRate / (exchanges.length - failed)).toFixed(1);
                $rate.text(exchangeRate + ' ' + currency).parents('div.info').css('visibility','visible');
                $paid.text(paid + ' (' + (paid * exchangeRate).toFixed(1) + ' ' + currency + ')');
                $next.text(next + ' (' + (next * exchangeRate).toFixed(1) + ' ' + currency + ')');
            });
    }

    function tryParseInt(str, defaultValue) {
        if (str !== undefined && str !== null && !isNaN(str))
            return parseInt(str);
        return defaultValue;
    }

    function tryParseFloat(str, defaultValue) {
        if (str !== undefined && str !== null && !isNaN(str))
            return parseFloat(str);
        return defaultValue;
    }

    function getQueryVariable(variable) {
        var query = window.location.search.substring(1),
            vars = query.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (decodeURIComponent(pair[0]) == variable)
                return decodeURIComponent(pair[1]);
        }
        return null;
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
});
